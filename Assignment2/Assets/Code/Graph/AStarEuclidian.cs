using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class AStarEuclidian : SearchAlgorithm
{
	protected override float CalculateH(Vertex child, Vertex goal)
	{
		return Vector3.Distance(child.transform.position, goal.transform.position);
	}
}
