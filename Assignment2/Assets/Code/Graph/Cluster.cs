﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Cluster : MonoBehaviour 
{
	public List<Vertex> clusterVertices = new List<Vertex>();

	public void InitGraph()
	{
		collider.enabled = true;
		float diagonal = (collider.bounds.max - collider.bounds.min).magnitude;

		List<Collider> vertices = Physics.OverlapSphere(this.transform.position, diagonal).ToList();
		vertices.RemoveAll( c => {
			return !c.bounds.Intersects(collider.bounds) || c.tag != "Vertex";
		});

		for(int i = 0; i < vertices.Count; i++)
		{
			clusterVertices.Add(vertices[i].GetComponent<Vertex>());
		}

		Destroy(collider);
		Destroy(rigidbody);
	}
}
