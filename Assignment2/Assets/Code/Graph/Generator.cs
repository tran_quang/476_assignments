﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Generator : MonoBehaviour 
{
	public List<Vertex> Vertices {get; private set;}
	public List<Cluster> Clusters {get; private set;}

	protected virtual void Awake()
	{
		Vertices = new List<Vertex>();
		Clusters = new List<Cluster>();
		GenerateLevel();
		Debug.Log("Number of nodes: " + Vertices.Count);
		
		InitClusters();

		PathFinder pathFinder = this.gameObject.AddComponent<PathFinder>();
		SpawnNPC(pathFinder);
	}

	protected abstract void GenerateLevel();

	private void SpawnNPC(PathFinder pathFinder)
	{
		GameObject.Instantiate(Resources.Load("Graph/SpawnPoints"));

		GameObject npc = Resources.Load("NPC/NPC") as GameObject;
		npc = GameObject.Instantiate(npc) as GameObject;
		
		pathFinder.movement = npc.GetComponent<Movement>();
		pathFinder.respawner = npc.GetComponent<Respawner>();
	}

	private void InitClusters()
	{
		GameObject[] clusters = GameObject.FindGameObjectsWithTag("Cluster");
		for(int i = 0; i < clusters.Length; i++)
		{
			Cluster c = clusters[i].GetComponent<Cluster>();
			c.InitGraph();
			Clusters.Add(c);
		}
	}
}
