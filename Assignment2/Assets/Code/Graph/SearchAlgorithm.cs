using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public abstract class SearchAlgorithm
{
	protected Dictionary<Vertex, float> openList = new Dictionary<Vertex, float>();
	protected HashSet<Vertex> closedList = new HashSet<Vertex>();

	protected Dictionary<Vertex, Vertex> connections = new Dictionary<Vertex, Vertex>();

	public List<Vertex> FindPath(Vertex start, Vertex end)
	{
		openList.Add(start, CalculateH(start, end));
		Vertex current = start;
		bool foundPath = false;

		while(openList.Count > 0)
		{
			// Find min f(x) = g(x) + h(x)
			current = openList.Aggregate((min, compare) => { 
				return compare.Value < min.Value ? compare : min; 
			}).Key;

			if(current == end)
			{
				foundPath = true;
				break;
			}

			List<Vertex> newVertices = current.neigbors.Except(closedList).ToList();
			for(int i = 0; i < newVertices.Count; i++)
			{
				float cost = CalculateG(current, newVertices[i], end) + CalculateH(newVertices[i], end);

				if((openList.ContainsKey(newVertices[i]) && cost < openList[newVertices[i]])
					|| !openList.ContainsKey(newVertices[i]))
				{
					openList[newVertices[i]] = cost;
					newVertices[i].VisitNode();
					connections[newVertices[i]] = current; // To trace backwards path
				}
			}
			
			openList.Remove(current);
			closedList.Add(current);
		}

		if(foundPath)
		{
			return findPath(start, end);
		}
		else
		{
			return null;
		}
	}

	protected abstract float CalculateH(Vertex child, Vertex goal);

	private float CalculateG(Vertex parent, Vertex child, Vertex end)
	{
		float startToParent = openList[parent] - CalculateH(parent, end);
		return startToParent + Vector3.Distance(parent.transform.position, child.transform.position);
	}

	private List<Vertex> findPath(Vertex start, Vertex end)
	{
		List<Vertex> path = new List<Vertex>();
		
		Vertex current = end;
		while(current != start)
		{
			path.Insert(0, current);
			current = connections[current];
		}
		path.Insert(0, start);
		
		for(int i = 1; i < path.Count - 1; i++)
		{
			path[i].PathNode();
		}
		
		start.StartNode();
		end.EndNode();
		
		return path;
	}

	public void Clear()
	{
		foreach(Vertex v in openList.Keys)
		{
			v.ResetNode();
		}
		
		foreach(Vertex v in closedList)
		{
			v.ResetNode();
		}
		
		openList.Clear();
		closedList.Clear();
		connections.Clear();
	}
}
