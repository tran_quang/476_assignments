﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AStarCluster : SearchAlgorithm 
{
	private Dictionary<Cluster, Dictionary<Cluster, float>> clusterLookups = new Dictionary<Cluster, Dictionary<Cluster, float>>();

	public AStarCluster()
	{
		GameObject[] clusterGameObjects = GameObject.FindGameObjectsWithTag("Cluster");
		Cluster[] clusters = new Cluster[clusterGameObjects.Length];

		for(int i = 0; i < clusterGameObjects.Length; i++)
		{
			clusters[i] = clusterGameObjects[i].GetComponent<Cluster>();
		}

		//GetNeighboringClusterDistances(clusters);
		PathFinder pathFinder = GameObject.Find("Graph").GetComponent<PathFinder>();
		pathFinder.enabled = false;
		pathFinder.StartCoroutine(Loading());
		pathFinder.StartCoroutine(CreateLookupTable(clusters, pathFinder));
	}

	protected override float CalculateH(Vertex child, Vertex goal)
	{
		float distance = ClusterLookup(child, goal);
		if(distance < 0)
		{
			return float.MaxValue;
		}
		else
		{
			return distance;
		}
	}

	private float ClusterLookup(Vertex child, Vertex goal)
	{
		ICollection<Cluster> clusters = clusterLookups.Keys;
		Cluster childCluster = clusters.First(c => { return c.clusterVertices.Contains(child); });
		Cluster goalCluster = clusters.First (c => { return c.clusterVertices.Contains(goal); });

		if(childCluster != goalCluster)
		{
			float clusterDistance;
			if(clusterLookups[childCluster].TryGetValue(goalCluster, out clusterDistance))
			{
				return clusterDistance;
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return Vector3.Distance(child.transform.position, goal.transform.position);
		}
	}

	private IEnumerator CreateLookupTable(Cluster[] clusters, PathFinder pathFinder)
	{
		float start = Time.realtimeSinceStartup;
		for(int i = 0; i < clusters.Length; i++)
		{
			if(!clusterLookups.ContainsKey(clusters[i]))
			{
				clusterLookups.Add(clusters[i], new Dictionary<Cluster, float>());
			}

			for(int j = 0; j < clusters.Length; j++)
			{
				Dictionary<Cluster, float> lookup;
				float distance;
				if(clusterLookups.TryGetValue(clusters[j], out lookup))
				{
					if(lookup.TryGetValue(clusters[i], out distance))
					{
						FillOpposite(clusters[i], clusters[j]);
					}
				}
				else
				{
					yield return pathFinder.StartCoroutine(ClusterDistance(clusters[i], clusters[j]));
				}
			}
		}

		pathFinder.enabled = true;
		float end = Time.realtimeSinceStartup;
		Debug.Log("Lookup creation time: " + (end - start));

		Text loading = GameObject.Find("LoadingText").GetComponent<Text>();
		loading.enabled = false;
	}

	private IEnumerator ClusterDistance(Cluster start, Cluster end)
	{
		SearchAlgorithm search = new AStarEuclidian();
		float distance = float.MaxValue;

		List<Vertex> startVertices = start.clusterVertices.Where( v => {
			return v.neigbors.Except(start.clusterVertices).Count() > 0; // Only test vertices that are at exits
		}).ToList();
		List<Vertex> endVertices = end.clusterVertices.Where( v => {
			return v.neigbors.Except(end.clusterVertices).Count() > 0;
		}).ToList();

		for(int i = 0; i < startVertices.Count; i++)
		{
			for(int j = 0; j < endVertices.Count; j++)
			{
				List<Vertex> path = search.FindPath(startVertices[i], endVertices[j]);
				if(path == null) // Skip if there is no path
				{
					continue;
				}

				float pathDistance = 0;
				for(int k = 1; k < path.Count; k++)
				{
					pathDistance += Vector3.Distance(path[k-1].transform.position, path[k].transform.position);
				}

				if(pathDistance < distance)
				{
					distance = pathDistance;
				}
				search.Clear();
			}
			yield return null;
		}

		if(distance > 0)
		{
			clusterLookups[start].Add(end, distance);
        }
	}

	private void FillOpposite(Cluster start, Cluster end)
	{
		float distance = clusterLookups[end][start];
		clusterLookups[start].Add(end, distance);
	}

	private IEnumerator Loading()
	{
		Text loading = GameObject.Find("LoadingText").GetComponent<Text>();
		bool addDots = false;

		while(loading.enabled)
		{
			if(addDots)
			{
				loading.text = loading.text + ".";
				addDots = loading.text.Length == 10 ? false : true;
			}
			else
			{
				loading.text = loading.text.Substring(0, loading.text.Length - 1);
				addDots = loading.text.Length == 7 ? true : false;
			}
			yield return new WaitForSeconds(0.5f);
		}
	}
}
