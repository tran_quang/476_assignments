using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileGenerator : Generator 
{
	public GameObject floor;
	public float tileDensity;

	protected override void GenerateLevel()
	{
		GameObject vertex = Resources.Load("Graph/Vertex") as GameObject;
		float bound = floor.renderer.bounds.max.x;

		float min = -bound + 0.5f/tileDensity;
		Vector3 vertexPosition = new Vector3(min, 0.05f, min);
		
		while(vertexPosition.z < bound)
		{
			while(vertexPosition.x < bound)
			{
				if(!Physics.CheckSphere(vertexPosition, 0.25f/tileDensity))
				{
					GameObject currentVertex = GameObject.Instantiate(vertex) as GameObject;
					currentVertex.transform.parent = this.transform;
					currentVertex.transform.position = vertexPosition;

					Vertices.Add(currentVertex.GetComponent<Vertex>());
				}
				else
				{
					Debug.Log("Vertex not generated at position " + vertexPosition);
				}

				vertexPosition.x += 1/tileDensity;
			}
			vertexPosition.x = min;
			vertexPosition.z += 1/tileDensity;
		}

		float maxNeighborDistance = Mathf.Sqrt(Mathf.Pow(1/tileDensity, 2) * 2);
		for(int i = 0; i < Vertices.Count; i++)
		{
			Vertices[i].InitGraph(maxNeighborDistance);
		}
	}
}
