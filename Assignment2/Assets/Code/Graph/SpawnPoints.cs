﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPoints : MonoBehaviour 
{
	public List<Vertex> locations = new List<Vertex>();

	private void OnTriggerStay(Collider c)
	{
		if(c.tag == "Vertex")
		{
			Vertex v = c.GetComponent<Vertex>();
			if(!locations.Contains(v))
			{
				locations.Add(v);
			}
		}
		Destroy(rigidbody); // So trigger isn't called anymore
	}
}
