﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PathFinder : MonoBehaviour
{
	public Vertex start;
	public Vertex end;
	public SearchType search;
	public Movement movement;
	public Respawner respawner;

	private Text heuristic;
	private Dictionary<SearchType, SearchAlgorithm> algorithms = new Dictionary<SearchType, SearchAlgorithm>();

	private void Start()
	{
		heuristic = GameObject.Find("HeuristicText").GetComponent<Text>();
		algorithms.Add(SearchType.DIJKSTRA, new DijkstraNullHeuristic());
		algorithms.Add(SearchType.A_STAR_EUCLIDIAN, new AStarEuclidian());
		algorithms.Add (SearchType.A_STAR_CLUSTER, new AStarCluster());
	}

	private void Update () 
	{
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit))
			{
				Vertex v = hit.collider.GetComponent<Vertex>();
				if(v != null)
				{
					end = v;
					end.EndNode();

					start = FindClosestNode(movement);
					if(start != null)
					{
						start.StartNode();
						foreach(SearchAlgorithm a in algorithms.Values)
						{
							a.Clear();
						}
						List<Vertex> path = algorithms[search].FindPath(start, end);
						if(path != null)
						{
							movement.path = path;
						}
					}
				}
			}
		}
		else if(Input.GetMouseButtonDown(1)) // For debugging algorithms
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit))
			{
				Vertex v = hit.collider.GetComponent<Vertex>();
				if(v != null)
				{
					start = v;
					start.StartNode();
				}
			}
		}
		else if(Input.GetKeyDown(KeyCode.Return))
		{
			RandomPath();
		}

		if(Input.GetKeyDown (KeyCode.A))
		{
			ToggleSearch();
        }
	}

	private Vertex FindClosestNode(Movement movement)
	{
		Collider[] hits = Physics.OverlapSphere(movement.transform.position, 5);
		float distance = float.MaxValue;
		Vertex closest = null;
		for(int i = 0; i < hits.Length; i++)
		{
			if(hits[i].tag != "Vertex")
			{
				continue;
			}

			Vector3 direction = hits[i].transform.position - movement.transform.position;
			if(direction.magnitude < distance)
			{
				distance = direction.magnitude;
				RaycastHit[] obstacles = Physics.RaycastAll(movement.transform.position, direction, direction.magnitude);
				bool obstacle = false;
				for(int j = 0; j < obstacles.Length; j++)
				{
					string tag = obstacles[j].collider.tag;
					if(tag != "Vertex") // Ignore hits with other vertices/cluster
					{
						obstacle = true;
					}
				}

				if(!obstacle)
				{
					closest = hits[i].GetComponent<Vertex>();
				}
			}
		}
		return closest;
	}

	private void ToggleSearch()
	{
		switch(search)
		{
		case SearchType.DIJKSTRA:
			search = SearchType.A_STAR_EUCLIDIAN;
			break;
		case SearchType.A_STAR_EUCLIDIAN:
			search = SearchType.A_STAR_CLUSTER;
			break;
		case SearchType.A_STAR_CLUSTER:
			search = SearchType.DIJKSTRA;
			break;
		}
		heuristic.text = search.ToString();
	}

	private void RandomPath()
	{
		start = respawner.Respawn();

		foreach(SearchAlgorithm a in algorithms.Values)
		{
			a.Clear();
		}

		start.StartNode();
		end = respawner.locations[UnityEngine.Random.Range(0, respawner.locations.Count)];
		end.EndNode();

		List<Vertex> path = algorithms[search].FindPath(start, end);
		if(path != null)
		{
			movement.path = path;
		}
	}
}

public enum SearchType { DIJKSTRA, A_STAR_EUCLIDIAN, A_STAR_CLUSTER, };
