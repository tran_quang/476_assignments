using System.Collections.Generic;
using System.Linq;

public class DijkstraNullHeuristic : SearchAlgorithm
{
	protected override float CalculateH(Vertex child, Vertex goal)
	{
		return 0;
	}
}
