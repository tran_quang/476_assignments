﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Vertex : MonoBehaviour 
{
	public List<Vertex> neigbors = new List<Vertex>();

	private SpriteRenderer spriteRenderer;

	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void InitGraph(float radius)
	{
		if(neigbors.Count == 0)
		{
			Collider[] c = Physics.OverlapSphere(this.transform.position, radius);
			for(int i = 0; i < c.Length; i++)
			{
				if(c[i].tag == "Vertex" && c[i] != this.collider)
				{
					Vertex v = c[i].GetComponent<Vertex>();
					Vector3 direction = v.transform.position - this.transform.position;
					RaycastHit[] hits = Physics.RaycastAll(this.transform.position, direction, direction.magnitude);

					bool obstacle = false;
					for(int j = 0; j < hits.Length; j++)
					{
						string tag = hits[j].collider.tag;
						if(tag != "Vertex" && tag != "Cluster") // Ignore hits with other vertices/cluster
						{
							obstacle = true;
						}
					}

					if(!obstacle)
					{
						neigbors.Add(v);
					}
				}
			}
		}
	}

	public void ResetNode()
	{
		spriteRenderer.color = Color.white;
	}

	public void VisitNode()
	{
		spriteRenderer.color = Color.yellow;
	}

	public void PathNode()
	{
		spriteRenderer.color = Color.blue;
	}

	public void EndNode()
	{
		spriteRenderer.color = Color.green;
	}

	public void StartNode()
	{
		spriteRenderer.color = Color.black;
	}
}
