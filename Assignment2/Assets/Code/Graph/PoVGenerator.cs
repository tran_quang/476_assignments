using UnityEngine;
using System.Collections;

public class PoVGenerator : Generator 
{
	public GameObject floor;

	protected override void GenerateLevel ()
	{
		GameObject[] vertexObjects = GameObject.FindGameObjectsWithTag("Vertex");
		for(int i = 0; i < vertexObjects.Length; i++)
		{
			Vertices.Add(vertexObjects[i].GetComponent<Vertex>());
		}

		float arenaSize = floor.renderer.bounds.max.x - floor.renderer.bounds.min.x;
		float diagonalSize = Mathf.Sqrt(arenaSize * arenaSize + arenaSize * arenaSize);
		for(int i = 0; i < Vertices.Count; i++)
		{
			Vertices[i].InitGraph(diagonalSize);
		}
	}
}
