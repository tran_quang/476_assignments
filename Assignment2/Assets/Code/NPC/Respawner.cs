﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Respawner : MonoBehaviour 
{
	public List<Vertex> locations;

	private Movement movement;

	private void Awake()
	{
		GameObject spawn = GameObject.FindGameObjectWithTag("Spawn");
		locations = spawn.GetComponent<SpawnPoints>().locations;
		movement = GetComponent<Movement>();
	}

	public Vertex Respawn()
	{
		movement.path = new List<Vertex>();
		Vertex v = locations[UnityEngine.Random.Range(0, locations.Count)];
		transform.position = new Vector3(v.transform.position.x, transform.position.y, v.transform.position.z);

		return v;
	}
}
