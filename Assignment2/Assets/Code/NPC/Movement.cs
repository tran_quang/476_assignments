﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Movement : MonoBehaviour 
{
	public const float NPC_WIDTH = 0.25f;

	public float acceleration;
	public float maxSpeed;
	private float speed = 0;
	public float slowDownRadius;
	public float rotationSpeed;

	public List<Vertex> path = new List<Vertex>();

	private void Update() 
	{
		if(path.Count > 0)
		{
			SteerTowards(path[0]);
		}
	}

	private void SteerTowards(Vertex v)
	{
		Vector3 target = v.transform.position;
		target.y = transform.position.y;

		if(Vector3.Distance(transform.position, target) < 0.05f)
		{
			path.RemoveAt(0);
			speed = 0;
			return;
		}

		Vector3 targetDirection = target - transform.position;
		float maxRadians = 2*Mathf.PI/360f * rotationSpeed;
		transform.forward = Vector3.RotateTowards(transform.forward, targetDirection, maxRadians * Time.deltaTime, 0);


		if(Vector3.Angle(targetDirection, transform.forward) > 15)
		{
			return;
		}

		float distance = Vector3.Distance(transform.position, target);
		if(distance > slowDownRadius)
		{
			speed = Mathf.Clamp(speed + acceleration * Time.deltaTime, 0, maxSpeed);
		}
		else
		{
			speed = Mathf.Clamp(speed - acceleration * Time.deltaTime, 0.5f, maxSpeed);
		}

		transform.position = Vector3.MoveTowards(transform.position, 
		                                         transform.position + transform.forward, 
		                                         speed * Time.deltaTime);
	}
}
