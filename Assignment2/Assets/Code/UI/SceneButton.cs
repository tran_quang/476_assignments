﻿using UnityEngine;
using System.Collections;

public class SceneButton : MonoBehaviour 
{
	public void ChangeScene()
	{
		int levelToLoad = (Application.loadedLevel + 1) % Application.levelCount;
		Application.LoadLevel(levelToLoad);
	}
}
