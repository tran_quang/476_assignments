﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	private const int NUM_GHOSTS = 3;

	public static GameManager Instance {get; private set;}

	public AudioClip intro;

	public Vector3 Spawn {get; private set;}
	public Vector3 GhostSpawn {get; private set;}
	public GameObject Player {get; set;}

	private void Awake() 
	{
		if(Instance == null)
		{
			Instance = this;
			Application.runInBackground = true;
			DontDestroyOnLoad(this.gameObject);
			Spawn = new Vector3(0f, 0.5f, -7.75f);
			GhostSpawn = new Vector3(0f, 0.5f, 4f);
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	public void ToggleControls(bool enabled)
	{
		Player.GetComponent<PlayerInput>().enabled = enabled;
	}

	public IEnumerator StartGame()
	{

		audio.PlayOneShot(intro);
		yield return new WaitForSeconds(intro.length);

		if(Network.isServer)
		{
			GameObject ghost = Resources.Load("Prefabs/Ghost") as GameObject;
			for(int i = 0; i < NUM_GHOSTS; i++)
			{
				Network.Instantiate(ghost, GhostSpawn, Quaternion.identity, 1);
			}
		}
	}
}
