﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour 
{
	public static NetworkManager Instance {get; private set;}

	private bool playerConnected;
	private GameObject waitText;

	private void Awake() 
	{
		if(Instance == null)
		{
			Instance = this;
			Application.runInBackground = true;
			DontDestroyOnLoad(this.gameObject);
			waitText = GameObject.Find("WaitText");
			waitText.SetActive(false);
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	private void OnConnectedToServer()
	{
		spawnPlayer();
	}

	private void OnPlayerConnected()
	{
		playerConnected = true;
	}

	private void OnPlayerDisconnected()
	{
		Network.DestroyPlayerObjects(Network.player);
	}

	private void OnServerInitialized()
	{
		spawnPlayer();
	}

	private IEnumerator waitForPlayer()
	{
		GameManager.Instance.ToggleControls(false);
		waitText.SetActive(true);
		while(!playerConnected)
		{
			yield return null;
		}
		waitText.SetActive(false);
		yield return StartCoroutine(GameManager.Instance.StartGame());
		GameManager.Instance.ToggleControls(true);
	}

	private void spawnPlayer()
	{
		Vector3 spawn = GameManager.Instance.Spawn;
		GameObject player = Resources.Load("Prefabs/Player") as GameObject;
		GameManager.Instance.Player = Network.Instantiate(player, spawn, Quaternion.identity, 1) as GameObject;

		playerConnected = Network.isClient;
		StartCoroutine(waitForPlayer());
	}
}
