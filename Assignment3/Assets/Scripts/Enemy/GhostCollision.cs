﻿using UnityEngine;
using System.Collections;

public class GhostCollision : MonoBehaviour 
{
	private void OnCollisionEnter(Collision c)
	{
		if(c.collider.tag == "Player")
		{
			if(c.collider.networkView.isMine)
			{
				c.collider.networkView.RPC("Respawn", RPCMode.All);
			}
			GhostMovement movement = GetComponent<GhostMovement>();
			if(movement != null)
			{
				movement.SetDestination(transform.position);
			}
		}
		if(c.collider.tag == "Ghost")
		{
			Physics.IgnoreCollision(c.collider, this.collider);
		}
	}
}
