﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Rigidbody))]
public class GhostMovement : MonoBehaviour 
{
	private const float MAX_SPEED = 4.5f;

	private HashSet<GameObject> players = new HashSet<GameObject>();
	private Vector3 destination;
	private Rigidbody rb;

	private void Awake() 
	{
		players.UnionWith(GameObject.FindGameObjectsWithTag("Player"));
		destination = transform.position;
		rb = GetComponent<Rigidbody>();

		if(!networkView.isMine)
		{
			Destroy(this);
		}
	}
	
	private void Update() 
	{
		GameObject target = players.Aggregate( (p1, p2) => {

			float toPlayer1 = Vector3.Distance(p1.transform.position, this.transform.position);
			float toPlayer2 = Vector3.Distance(p2.transform.position, this.transform.position);

			return toPlayer1 > toPlayer2 ? p2 : p1;
		});

		Vector3 towardsPlayer = target.transform.position - this.transform.position;
		Ray ray = new Ray(this.transform.position, towardsPlayer);
		RaycastHit[] hits = Physics.SphereCastAll(ray, 0.25f, towardsPlayer.magnitude);
		bool targetable = true;
		for(int i = 0; i < hits.Length; i++)
		{
			if(hits[i].collider.tag == "Untagged")
			{
				targetable = false;
			}
		}

		if(targetable)
		{
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit) && hit.collider.tag == "Destination");
			{
				destination = hit.transform.position;
				destination.y = transform.position.y;
			}
		}

		Vector3 direction = destination - this.transform.position;
		if(direction.magnitude > 0.1f)
		{
			rb.velocity = direction.normalized * MAX_SPEED;
			if(direction.magnitude > 1f)
			{
				transform.forward = direction.normalized;
			}
		}
		else
		{
			findDestination();
		}
	}

	private void findDestination()
	{
		Vector3 direction = destination - this.transform.position;
		while(direction.magnitude < 0.1f || Vector3.Angle(transform.forward, direction) > 100)
		{
			float rotation = UnityEngine.Random.Range(0, 4) * 90;
			Vector3 forward = Quaternion.AngleAxis(rotation, Vector3.up) * Vector3.forward;

			Ray ray = new Ray(this.transform.position, forward);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit) && hit.collider.tag == "Destination")
			{
				destination = hit.transform.position;
			}
			direction = destination - this.transform.position;
		}

		destination.y = this.transform.position.y;
	}

	public void SetDestination(Vector3 destination)
	{
		this.destination = destination;
	}
}
