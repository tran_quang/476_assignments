﻿using UnityEngine;
using System.Collections;

public class Dot : MonoBehaviour 
{
	private void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player")
		{
			Destroy(this.gameObject);
			c.GetComponent<PlayerScore>().AddScore();
		}
	}
}
