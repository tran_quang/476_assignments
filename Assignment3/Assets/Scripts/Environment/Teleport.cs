﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour 
{
	public Teleport endPoint;
	public GameObject destination;
	public Vector3 offset;

	private void Start()
	{
		RaycastHit hit;
		if(Physics.Raycast(endPoint.transform.position, endPoint.offset, out hit))
		{
			destination = hit.collider.gameObject;
		}
	}

	private void OnTriggerEnter(Collider c) 
	{
		if(c.tag == "Player" || c.tag == "Ghost")
		{
			c.transform.position = endPoint.transform.position + endPoint.offset;

			PlayerMovement movement = c.GetComponent<PlayerMovement>();
			movement.SetDestination(destination.transform.position);
		}
	}
}
