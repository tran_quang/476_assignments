﻿using UnityEngine;
using System.Collections;

public class PowerDot : MonoBehaviour 
{
	private Renderer[] renderers;

	private void Awake()
	{
		renderers = GetComponentsInChildren<Renderer>();
		InvokeRepeating("Flash", 0f, 0.25f);
	}

	private void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player")
		{
			PlayerMovement movement = c.GetComponent<PlayerMovement>();
			movement.StartCoroutine(movement.PowerUp());
		}
	}

	private void Flash()
	{
		for(int i = 0; i < renderers.Length; i++)
		{
			renderers[i].enabled = !renderers[i].enabled;
		}
	}
}
