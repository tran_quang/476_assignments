﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(SphereCollider))]
public class PlayerInput : MonoBehaviour 
{
	private PlayerMovement movement;
	private SphereCollider sphereCollider;

	private void Awake()
	{
		movement = GetComponent<PlayerMovement>();
		sphereCollider = GetComponent<SphereCollider>();
		if(!networkView.isMine)
		{
			Destroy(this);
		}
	}

	private void Update() 
	{
		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");

		RaycastHit hit;
		if(horizontal != 0 && vertical == 0)
		{
			Ray ray = new Ray(transform.position, Vector3.right * horizontal);
			if(Physics.SphereCast(ray, sphereCollider.radius, out hit) 
			   && hit.collider.tag == "Destination")
			{
				Vector3 destination = hit.collider.gameObject.transform.position;
				movement.networkView.RPC("SetDestination", RPCMode.All, destination);
			}
		}
		if(vertical != 0 && horizontal == 0)
		{
			Ray ray = new Ray(transform.position, Vector3.forward * vertical);
			if(Physics.SphereCast(ray, sphereCollider.radius, out hit) 
			   && hit.collider.tag == "Destination")
			{
				Vector3 destination = hit.collider.gameObject.transform.position;
				movement.networkView.RPC("SetDestination", RPCMode.All, destination);
			}
		}
	}
}
