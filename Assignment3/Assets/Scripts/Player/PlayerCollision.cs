﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour 
{
	private void OnCollisionEnter(Collision c)
	{
		if(c.collider.tag == "Player")
		{
			Physics.IgnoreCollision(c.collider, this.collider);
		}
	}
}
