﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour 
{
	public AudioClip death;

	private float maxSpeed = 5f;
	private Vector3 destination;
	private Rigidbody rb;
	private bool vulnerable = true;

	private void Awake()
	{
		destination = this.transform.position;
		rb = this.GetComponent<Rigidbody>();
	}

	private void Update() 
	{
		Vector3 direction = destination - this.transform.position;
		if(direction.magnitude > 0.1f)
		{
			rb.velocity = direction.normalized * maxSpeed;
			if(direction.magnitude > 1f)
			{
				transform.forward = direction.normalized;
			}
		}
		else
		{
			RaycastHit hit;
			if(Physics.Raycast(transform.position, transform.forward, out hit)
			   && hit.collider.tag == "Destination")
			{ 
				SetDestination(hit.transform.position);
			}
			else
			{
				rb.velocity = Vector3.zero;
			}
		}

		Debug.DrawLine(transform.position, destination);
	}

	public IEnumerator PowerUp()
	{
		maxSpeed = maxSpeed*1.5f;
		float speedTimer = 5f;
		while(speedTimer > 0)
		{
			yield return null;
			speedTimer -= Time.deltaTime;
		}
		maxSpeed = maxSpeed/1.5f;
	}

	[RPC]
	public void SetDestination(Vector3 destination)
	{
		destination.y = this.transform.position.y;
		this.destination = destination;
	}

	[RPC]
	public void Respawn()
	{
		if(vulnerable)
		{
			audio.PlayOneShot(death);
			transform.position = GameManager.Instance.Spawn;
			rb.velocity = Vector3.zero;
			destination = transform.position;
			StartCoroutine(flashAndLock());
		}
	}

	private IEnumerator flashAndLock()
	{
		vulnerable = false;

		float deathTimer = 2.5f;
		bool visible = false;
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		while(deathTimer > 0)
		{
			deathTimer -= 0.5f;
			for(int i = 0; i < renderers.Length; i++)
			{
				renderers[i].enabled = visible;
			}
			visible = !visible;
			yield return new WaitForSeconds(0.5f);
		}

		for(int i = 0; i < renderers.Length; i++)
		{
			renderers[i].enabled = true;
		}

		vulnerable = true;
	}
}
