﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerScore : MonoBehaviour 
{
	private Text scoreText;
	private int score = 0;

	private void Awake()
	{
		if(Network.isServer && networkView.isMine || (Network.isClient && !networkView.isMine))
		{
			scoreText = GameObject.Find("ServerScore").transform.GetChild(0).GetComponent<Text>();
		}
		else if((Network.isClient && networkView.isMine) || (Network.isServer && !networkView.isMine))
		{
			scoreText = GameObject.Find("ClientScore").transform.GetChild(0).GetComponent<Text>();
		}
	}
	
	public void AddScore()
	{
		scoreText.text = (++score).ToString();
		if(!audio.isPlaying)
		{
			audio.Play();
		}
	}
}
