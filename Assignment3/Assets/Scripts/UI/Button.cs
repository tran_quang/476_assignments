﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Button : MonoBehaviour 
{
	public static string IP = "127.0.0.1:9001";
	private static List<GameObject> uiElements;

	private void Awake()
	{
		if(uiElements == null)
		{
			uiElements = new List<GameObject>();
			for(int i = 0; i < transform.parent.childCount; i++)
			{
				if(transform.parent.GetChild(i).tag == "Hideable")
				{
					uiElements.Add(transform.parent.GetChild(i).gameObject);
				}
			}
		}
	}

	public void Connect()
	{
		string[] ipPort = IP.Split(':');
		Network.Connect(ipPort[0], int.Parse(ipPort[1]));
		disableUI();
	}

	public void Create()
	{
		int port = int.Parse(IP.Split(':')[1]);
		Network.InitializeServer(1, port, !Network.HavePublicAddress());
		disableUI();
	}

	private void disableUI()
	{
		for(int i = 0; i < uiElements.Count; i++)
		{
			uiElements[i].SetActive(false);
		}
	}
}
