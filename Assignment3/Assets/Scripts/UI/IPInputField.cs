﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[RequireComponent(typeof(InputField))]
public class IPInputField : MonoBehaviour 
{
	private InputField input;

	private void Awake()
	{
		input = GetComponent<InputField>();
	}

	public void UpdateIP()	
	{
		Button.IP = input.text;
	}
}
