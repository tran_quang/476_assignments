﻿using UnityEngine;
using System.Collections;

public interface ButtonListener
{
	void Notify(string button);
}
