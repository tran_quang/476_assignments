﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TinyWander : TinyState 
{
	private static readonly float TIMER = 1.5f;

	// Movement variables
	private float speed = 1.0f;
	private float rotationMax = 90f;	
	private float degreesPerSecond = 50.0f;
	private float timer = 0f;
	private Quaternion finalRotation;

	// Positioning variables
	private List<Collider> baseBorders;

	public TinyWander(TinyController warrior) : base(warrior)
	{
		finalRotation = warrior.transform.rotation;
		warrior.GetComponentInChildren<Animator>().SetFloat("Speed", speed);

		baseBorders = new List<Collider>(3);
		GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");

		for(int i = 0; i < walls.Length; i++)
		{
			if(walls[i].GetComponent<BaseBorder>() != null)
			{
				baseBorders.Add(walls[i].collider);
			}
		}
	}
	
	public override void UpdatePosition() 
	{
		Vector3 direction, exit;
		findClosestExit(out direction, out exit);

		if(warrior.IsHome) // Keep direction towards home base if in enemy base
		{
			if(direction.magnitude > 1.25f)
			{
				timer -= Time.deltaTime;
				if(timer < 0)
				{
					timer = TIMER;
					ChangeDirection();
				}
			}
			else
			{
				finalRotation = Quaternion.LookRotation(-direction);
			}
		}
		else
		{
			finalRotation = Quaternion.LookRotation(direction);
		}

		warrior.transform.rotation = Quaternion.RotateTowards(warrior.transform.rotation, finalRotation, Time.deltaTime * degreesPerSecond);
		warrior.rigidbody.velocity = warrior.transform.forward * speed;
		warrior.GetComponentInChildren<Animator>().SetFloat("Speed", speed);
	}

	private void ChangeDirection()
	{
		if(!warrior.IsHome) // Keep the direction to go back to base
		{
			return;
		}

		Vector3 rotation = warrior.transform.rotation.eulerAngles;
		rotation.y += 2 * (Random.value - 0.5f) * rotationMax;
		finalRotation = Quaternion.Euler(rotation);
	}

	private void findClosestExit(out Vector3 direction, out Vector3 exit)
	{
		exit = baseBorders[0].ClosestPointOnBounds(warrior.transform.position);
		direction = Vector3.ProjectOnPlane(exit - warrior.transform.position, Vector3.up);

		for(int i = 1; i < baseBorders.Count; i++)
		{
			Vector3 otherPoint = baseBorders[i].ClosestPointOnBounds(warrior.transform.position);
			Vector3 otherDirection = Vector3.ProjectOnPlane(otherPoint - warrior.transform.position, Vector3.up);
			
			if(otherDirection.magnitude < direction.magnitude)
			{
				direction = otherDirection;
				exit = otherPoint;
			}
		}
	}
}
