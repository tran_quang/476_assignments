using UnityEngine;
using System.Collections;

public abstract class TinyState 
{
	protected TinyController warrior { get; set; }

	public TinyState(TinyController warrior)
	{
		this.warrior = warrior;
	}

	public abstract void UpdatePosition();

	public virtual void Tag(TinyController other) 
	{
		if(other.gameObject == warrior.target) // Tagged an enemy/ally
		{
			warrior.ChangeState(StateKey.WANDER);
		}
		else if(!warrior.IsHome && other.team != warrior.team)
		{
			warrior.DropFlag();
			warrior.ChangeState(StateKey.FROZEN);
		}
	}

	public virtual void CrossBorder() 
	{
		warrior.DropFlag();
	}

	public virtual void OnEnter() {}

	public virtual void OnExit() {}
}
