﻿using UnityEngine;
using System.Collections;

public class TinySteeringArrive : TinyState 
{
	private Animator animator;
	private float radiusOfSatisfaction = 0.1f;
	private float slowDownRadius = 1f;
	private float maxSpeed = 1f;
	private float maxSlowDown = 0.8f;
	private float acceleration = 0.5f;

	private Vector3 velocity = Vector3.zero;
	private Vector3 counterVelocity = Vector3.zero;

	public TinySteeringArrive(TinyController warrior) : base(warrior)
	{
		animator = warrior.GetComponentInChildren<Animator>();
	}

	public override void UpdatePosition()
	{
		Vector3 direction = warrior.target.transform.position - warrior.transform.position;
		warrior.transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(warrior.transform.forward, direction, Time.deltaTime, 0));
		if(Vector3.Angle(direction, warrior.transform.forward) > 30)
		{
			velocity *= 0.5f; // Slow down a lot to change directions
		}
		else
		{
			float distance = direction.magnitude;
			if(velocity.magnitude > maxSpeed)
			{
				velocity = direction.normalized * maxSpeed;
			}
			else
			{
				velocity += direction.normalized * acceleration * Time.deltaTime;
			}

			if(distance < slowDownRadius) // Inside slow down area
			{
				if(counterVelocity.magnitude > maxSlowDown)
				{
					counterVelocity = -direction.normalized * maxSlowDown;
				}
				else
				{
					counterVelocity -= direction.normalized * acceleration * 0.8f * Time.deltaTime;
				}
			}
			else if(distance < radiusOfSatisfaction)
			{
				warrior.transform.position = warrior.target.transform.position;
				warrior.rigidbody.velocity = Vector3.zero;
			}
		}
		warrior.rigidbody.velocity = velocity + counterVelocity;
		animator.SetFloat("Speed", warrior.rigidbody.velocity.magnitude);
	}

	public override void OnEnter() 
	{
		velocity = Vector3.zero;
		counterVelocity = Vector3.zero;
	}
}
