﻿using UnityEngine;
using System.Collections;

public class TinySteeringSeek : TinyState 
{
	private Animator animator;
	private float radiusOfSatisfaction = 0.1f;
	private float maxSpeed = 1f;
	private float acceleration = 0.5f;

	public TinySteeringSeek(TinyController warrior) : base(warrior) 
	{
		animator = warrior.GetComponentInChildren<Animator>();
	}
	
	public override void UpdatePosition() 
	{
		Vector3 direction = warrior.target.transform.position - warrior.transform.position;
		warrior.transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(warrior.transform.forward, direction, Time.deltaTime, 0));
		if(Vector3.Angle(direction, warrior.transform.forward) > 30)
		{
			warrior.rigidbody.velocity *= 0.5f; // Slow down a lot to change directions
		}
		else
		{
			float distance = direction.magnitude;
			if(distance > radiusOfSatisfaction)
			{
				if(warrior.rigidbody.velocity.magnitude > maxSpeed)
				{
					warrior.rigidbody.velocity = direction.normalized * maxSpeed;
				}
				else
				{
					warrior.rigidbody.velocity += direction.normalized * acceleration * Time.deltaTime;
				}
			}
			else
			{
				warrior.transform.position = warrior.target.transform.position;
				warrior.rigidbody.velocity = Vector3.zero;
			}
		}
		animator.SetFloat("Speed", warrior.rigidbody.velocity.magnitude);
	}
}
