using UnityEngine;
using System.Collections;

public class TinyKinematicFlee : TinyState
{
	private Animator animator;
	private float radiusOfSatisfaction = 0.1f;
	private float maxSpeed = 1f;
	
	public TinyKinematicFlee(TinyController warrior) : base(warrior)
	{
		animator = warrior.GetComponentInChildren<Animator>();
	}
	
	public override void UpdatePosition()
	{
		Vector3 direction = (warrior.transform.position - warrior.target.transform.position).normalized;
		float distance = direction.magnitude;

		warrior.transform.forward = Vector3.RotateTowards(warrior.transform.forward, direction, Time.deltaTime, 0);
		
		if(distance > radiusOfSatisfaction)
		{
			if(Vector3.Angle(direction, warrior.transform.forward) < 1)
		   	{
				warrior.rigidbody.velocity = direction * maxSpeed;
				animator.SetFloat("Speed", maxSpeed);
			}
			else
			{
				warrior.rigidbody.velocity = Vector3.zero;
				animator.SetFloat("Speed", 0);
			}
		}
		else
		{
			warrior.transform.position = warrior.transform.position + direction;
			warrior.rigidbody.velocity = Vector3.zero;
		}
	}
}
