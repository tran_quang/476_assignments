﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Kinematic seek state used to chase other players.
/// </summary>
public class TinyKinematicSeek : TinyState 
{
	private Animator animator;
	private float radiusOfSatisfaction = 0.1f;
	private float maxSpeed = 1f;

	public TinyKinematicSeek(TinyController warrior) : base(warrior)
	{
		animator = warrior.GetComponentInChildren<Animator>();
	}
	
	public override void UpdatePosition ()
	{
		Vector3 direction = warrior.target.transform.position - warrior.transform.position;
		float distance = direction.magnitude;
		
		if(distance > radiusOfSatisfaction)
		{
			warrior.rigidbody.velocity = direction.normalized * maxSpeed;
			animator.SetFloat("Speed", maxSpeed);
		}
		else
		{
			warrior.transform.position = warrior.target.transform.position;
			warrior.rigidbody.velocity = Vector3.zero;
		}
		
		warrior.transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(warrior.transform.forward, direction, Time.deltaTime, 0));
	}
}
