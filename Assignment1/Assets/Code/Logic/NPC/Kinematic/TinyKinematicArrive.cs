using UnityEngine;
using System.Collections;

public class TinyKinematicArrive : TinyState
{
	private Animator animator;
	private float radiusOfSatisfaction = 0.1f;
	private float maxSpeed = 1f;

	public TinyKinematicArrive(TinyController warrior) : base(warrior)
	{
		animator = warrior.GetComponentInChildren<Animator>();
	}

	public override void UpdatePosition()
	{
		Vector3 direction = warrior.target.transform.position - warrior.transform.position;
		float distance = direction.magnitude;

		if(distance > radiusOfSatisfaction)
		{
			float speed = Mathf.Min(maxSpeed, distance/maxSpeed);

			warrior.rigidbody.velocity = direction.normalized * speed;
			animator.SetFloat("Speed", speed);
		}
		else
		{
			warrior.transform.position = warrior.target.transform.position;
			warrior.rigidbody.velocity = Vector3.zero;
		}

		warrior.transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(warrior.transform.forward, direction, Time.deltaTime, 0));
	}
}
