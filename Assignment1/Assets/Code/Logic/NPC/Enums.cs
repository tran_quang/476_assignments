public enum Teams
{
	RED,
	BLUE,
}

public enum StateKey
{
	WANDER,
	ARRIVE,
	SEEK,
	FLEE,
	FROZEN
}
