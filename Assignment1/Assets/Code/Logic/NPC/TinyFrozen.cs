using UnityEngine;
using System.Collections;

public class TinyFrozen : TinyState 
{
	public TinyFrozen(TinyController warrior) : base(warrior)
	{
	}

	public override void UpdatePosition() 
	{
		warrior.GetComponentInChildren<Animator>().SetFloat("Speed", 0);
		warrior.rigidbody.velocity = Vector3.zero;
	}

	public override void Tag(TinyController other)
	{
		if(other.team == warrior.team)
		{
			warrior.ChangeState(StateKey.WANDER);
		}
	}

	public override void OnEnter()
	{
		warrior.Frozen = true;
		warrior.MyTeam.GotFrozen(warrior);
	}

	public override void OnExit()
	{
		warrior.Frozen = false;
		warrior.MyTeam.GotFreed(warrior);
	}
}
