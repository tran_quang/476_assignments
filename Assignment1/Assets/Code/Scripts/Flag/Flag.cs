﻿using UnityEngine;
using System.Collections;

public class Flag : MonoBehaviour 
{
	public Teams team;

	private GameObject holder;
	private Vector3 originalPosition;

	private void Awake()
	{
		originalPosition = this.transform.position;
	}

	private void Update()
	{
		if(holder != null)
		{
			this.transform.position = holder.transform.position + Vector3.up * (holder.collider.bounds.max.y + renderer.bounds.max.y/2);
		}
	}

	public void Drop()
	{
		holder = null;
		this.transform.position = originalPosition;
		collider.enabled = true;
	}

	public void OnTriggerEnter(Collider c)
	{
		TinyController character = c.GetComponent<TinyController>();
		if(character != null && character.team != this.team)
		{
			holder = c.gameObject;
			collider.enabled = false;
			c.SendMessage("TakeFlag", this);
		}
	}
}
