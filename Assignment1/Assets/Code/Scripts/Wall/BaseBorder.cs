﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseBorder : MonoBehaviour 
{
	private void OnTriggerExit(Collider c) 
	{
		c.SendMessage("CrossBorder", SendMessageOptions.DontRequireReceiver);
	}
}
