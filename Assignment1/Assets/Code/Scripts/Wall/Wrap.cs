﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wrap : MonoBehaviour 
{
	public bool flipX = false;
	public bool flipZ = false;

	private const float OFFSET = 0.3f;

	private void OnTriggerExit(Collider c)
	{
		if(c.tag == "Player")
		{
			Vector3 position = c.transform.position;
			if(flipX)
			{
				int sign = position.x > 0 ? 1 : -1;
				position.x = -position.x + (sign * OFFSET);
			}
			else if(flipZ)
			{
				int sign = position.z > 0 ? 1 : -1;
				position.z = -position.z + (sign * OFFSET);
			}
			c.transform.position = position;
		}
	}
}
