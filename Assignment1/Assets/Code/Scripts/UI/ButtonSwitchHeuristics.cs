﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButtonSwitchHeuristics : MonoBehaviour 
{
	public static string Name = "Switch Heuristics";

	private List<ButtonListener> listeners = new List<ButtonListener>();

	public void Awake()
	{
		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		for(int i = 0; i < players.Length; i++)
		{
			listeners.Add(players[i].GetComponent<TinyController>());
		}
	}

	public void AddListener(ButtonListener listener)
	{
		listeners.Add(listener);
	}

	public void SwitchHeuristics()
	{
		for(int i = 0; i < listeners.Count; i++)
		{
			listeners[i].Notify(Name);
		}
	}
}
