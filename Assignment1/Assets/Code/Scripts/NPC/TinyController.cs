﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TinyController : MonoBehaviour, ButtonListener
{
	// Variables to store and change states
	private static readonly string KIN = "Kinematic";
	private static readonly string STE = "Steering";

	// Red or blue team
	public Teams team;
	public GroupController OtherTeam { get; set; }
	public GroupController MyTeam { get; set; }

	// Variables to change states
	public bool Busy ;
	public bool IsHome ;
	public bool Frozen ;
	private TinyState currentState;
	private string heuristic;
	private Dictionary<string, TinyState> states = new Dictionary<string, TinyState>();

	// Flag information to drop/hold/chase
	private Flag flag;

	// Current target
	public GameObject target;

	private void Awake() 
	{
		flag = null;

		// Kinematic states
		states.Add(KIN + StateKey.FLEE, new TinyKinematicFlee(this));
		states.Add(KIN + StateKey.ARRIVE, new TinyKinematicArrive(this));
		states.Add(KIN + StateKey.SEEK, new TinyKinematicSeek(this));
		states.Add(KIN + StateKey.WANDER, new TinyWander(this));
		states.Add(KIN + StateKey.FROZEN, new TinyFrozen(this));

		// Steering states
		states.Add(STE + StateKey.SEEK, new TinySteeringSeek(this));
		states.Add(STE + StateKey.ARRIVE, new TinySteeringArrive(this));
		states.Add(STE + StateKey.WANDER, states[KIN + StateKey.WANDER]);
		states.Add(STE + StateKey.FROZEN, states[KIN + StateKey.FROZEN]);

		// Default to steering and wander
		heuristic = STE;
		currentState = states[heuristic + StateKey.WANDER];
		Busy = false;
		IsHome = true;
	}
	
	private void Update () 
	{
		currentState.UpdatePosition();
	}

	public void OnTriggerEnter(Collider c)
	{
		TinyController otherPlayer = c.GetComponent<TinyController>();
		if(otherPlayer != null)
		{
			currentState.Tag(otherPlayer);
		}
	}

	public void TakeFlag(Flag flag)
	{
		this.flag = flag;
		MyTeam.GetFlag = false;
		ChangeState(StateKey.WANDER);
	}

	public void DropFlag()
	{
		if(flag != null)
		{
			flag.Drop();
			MyTeam.GetFlag = true;
		}
		this.flag = null;
	}

	public void ChangeState(StateKey key, GameObject target = null)
	{
		Busy = key != StateKey.WANDER; // Chasing/Fleeing are busy states
		currentState.OnExit();

		this.target = target;
		currentState = states[heuristic + key];
		currentState.OnEnter();
	}

	public void CrossBorder()
	{
		IsHome = !IsHome;
		currentState.CrossBorder();
		OtherTeam.CrossBorder(this);
	}

	public void Notify(string button)
	{
		if(button == ButtonSwitchHeuristics.Name)
		{
			heuristic = heuristic == KIN ? STE : KIN;
		}
	}
}
