﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GroupController : MonoBehaviour 
{
	public Teams team;
	public bool GetFlag { get; set; }

	private List<TinyController> characters = new List<TinyController>();
	private GameObject targetFlag;

	private List<GameObject> frozenCharacters = new List<GameObject>();
	private List<TinyController> invadingCharacters = new List<TinyController>();

	private void Awake() 
	{
		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		for(int i = 0; i < players.Length; i++)
		{
			TinyController character = players[i].GetComponent<TinyController>();
			if(character.team == this.team)
			{
				characters.Add(character);
				character.MyTeam = this;
			}
			else
			{
				character.OtherTeam = this;
			}
		}

		targetFlag = GameObject.FindGameObjectsWithTag("Flag").First((go) => { return go.GetComponent<Flag>().team != this.team; });
	}

	private void Start()
	{
		int index = Random.Range(0, characters.Count);
		characters[index].ChangeState(StateKey.ARRIVE, targetFlag);
		GetFlag = true;
	}

	private void Update()
	{
		if(GetFlag)
		{
			bool flagChaser = characters.Exists((character) => { return character.target == targetFlag; });
			if(!flagChaser)
			{
				TinyController c = findClosestAvailable(targetFlag.transform.position);
				if(c != null)
				{
					c.ChangeState(StateKey.ARRIVE, targetFlag);
				}
			}
		}

		for(int i = 0; i < invadingCharacters.Count; i++)
		{
			if(!invadingCharacters[i].Frozen)
			{
				bool invadingChaser = characters.Exists((character) => { return character.target == invadingCharacters[i].gameObject; });
				if(!invadingChaser)
				{
					TinyController c = findClosestAvailable(invadingCharacters[i].transform.position);
					if(c != null)
					{
						c.ChangeState(StateKey.SEEK, invadingCharacters[i].gameObject);
					}
				}
			}
		}

		for(int i = 0; i < frozenCharacters.Count; i++)
		{
			bool frozenChaser = characters.Exists((character) => { return character.target == frozenCharacters[i]; });
			if(!frozenChaser)
			{
				TinyController c = findClosestAvailable(frozenCharacters[i].transform.position);
				if(c != null)
				{
					c.ChangeState(StateKey.ARRIVE, frozenCharacters[i]);
				}
			}
		}
	}

	public void GotFrozen(TinyController character)
	{
		frozenCharacters.Add(character.gameObject);
	}

	public void GotFreed(TinyController character)
	{
		frozenCharacters.Remove(character.gameObject);
	}

	public void CrossBorder(TinyController opponent)
	{
		if(opponent.team != this.team)
		{
			if(!opponent.IsHome)
			{
				invadingCharacters.Add(opponent);
			}
			else
			{
				invadingCharacters.Remove(opponent);
				TinyController chaser = characters.FirstOrDefault((c) => { return c.target == opponent.gameObject; });
				if(chaser != null)
				{
					chaser.ChangeState(StateKey.WANDER);
				}
			}
		}
	}

	private TinyController findClosestAvailable(Vector3 point)
	{
		TinyController character = characters.FirstOrDefault((c) => { return !c.Busy && c.IsHome; });
		if(character != null)
		{
			float distance = Vector3.Distance(point, character.transform.position);
			
			for(int i = 0; i < characters.Count; i++)
			{
				if(!characters[i].Busy)
				{
					TinyController compareController = characters[i];
					float compareDistance = Vector3.Distance(point, compareController.transform.position);
					
					if(compareDistance < distance)
					{
						distance = compareDistance;
						character = compareController;
					}
				}
			}
		}

		return character;
	}
}
